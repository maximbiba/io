package com.iostreams;

import com.iostreams.services.FileServices;
import com.iostreams.wrappers.BufferedReaderWrapper;
import com.iostreams.wrappers.FileWriterWrapper;

public class Main {

    public static void main(String[] args) {
        FileServices fileServices = new FileServices(new BufferedReaderWrapper(), new FileWriterWrapper());
        String[] fileNames = {"DataCache\\old_file_1.txt",
        "DataCache\\old_file_2.txt",
        "DataCache\\old_file_3.txt"};

        String name = "DataCache/new_file_2.txt";

        String[] n = {name};

        fileServices.readFiles(n);
    }
}
